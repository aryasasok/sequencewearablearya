package com.example.sequencephotonapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {
    //demo acceleration value ffrom the photon.
    String acclerationvalues = "";
    // MARK: Debug info
    private final String TAG = "arya";

    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "aryasasok@gmail.com";
    private final String PARTICLE_PASSWORD = "arya@123";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "270018001047363333343437";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;
    private TextView tvSequence;
    private TextView tvResult;
    private EditText etInput;
    private Button btnGetResults;
    private Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();

        etInput = findViewById(R.id.et_input);
        tvSequence = findViewById(R.id.tv_sequence);
        tvResult = findViewById(R.id.tvResult);
        btnGetResults = findViewById(R.id.btn_get_results);
        btnStart = findViewById(R.id.btn_start);

        Button btnRestart = findViewById(R.id.btn_restart);
        btnRestart.setOnClickListener(view -> {
            finish();
            startActivity(new Intent(MainActivity.this,WelcomeActivity.class));
        });
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!etInput.getText().toString().equals("")) {
                    getSequenceData();
                } else {
                    Toast.makeText(MainActivity.this, "Please enter input sequence", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnGetResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getResults();
            }
        });
    }

    private void getSequenceData() {
        if (mDevice == null) {
            Log.d("TAG", "cannot find device");
            return;
        }
        start("start");
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
                        "output",  // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, final ParticleEvent event) {
                                runOnUiThread(new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tvSequence.setText("" + event.dataPayload);

                                        if(etInput.getText().toString().equalsIgnoreCase(event.dataPayload)) {
                                            // Win
                                            tvResult.setText("You Win");
                                        } else {
                                            // Lose
                                            tvResult.setText("You Lose");
                                        }
                                    }
                                }));
                            }


                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got data from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }

    public void start(final String status) {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {

                // 2. build a list and put the r,g,b into the list
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add(status);

                // 3. send the command to the particle
                try {
                    mDevice.callFunction("sequence", functionParameters);
                } catch (ParticleDevice.FunctionDoesNotExistException e) {
                    e.printStackTrace();
                }

                return -1;
            }


            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "command [" + status + "] sent to particle");
            }


            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });


    }

    private void getResults() {
        if (mDevice == null) {
            Log.d("TAG", "cannot find device");
            return;
        }
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                subscriptionId = ParticleCloudSDK.getCloud().subscribeToDeviceEvents("Result", DEVICE_ID,
                        // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, final ParticleEvent event) {
                                Log.d("Result", event.dataPayload);
                                runOnUiThread(new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        etInput.setText("" + event.dataPayload);
                                    }
                                }));
                            }

                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got data from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }

    /**
     * Custom function to connect to the Particle Cloud and get the device
     */
    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull final ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

}
