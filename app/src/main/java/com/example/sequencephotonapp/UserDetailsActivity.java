package com.example.sequencephotonapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class UserDetailsActivity extends AppCompatActivity {

    public static final String PREF_NAME = "preference";
    public static String KEY_USERNAME = "key_username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        SharedPreferences preferences = getSharedPreferences(PREF_NAME,MODE_PRIVATE);
        String userName = preferences.getString(KEY_USERNAME, "");
        if (!userName.isEmpty()) {
            startActivity(new Intent(this,WelcomeActivity.class));
            finish();
        }

        Button btnSave = findViewById(R.id.btn_save);
        EditText etUsername = findViewById(R.id.et_user_name);
        btnSave.setOnClickListener(view -> {
            String name = etUsername.getText().toString();
            if (name.isEmpty()) {
                etUsername.setError("Invalid username");
            } else {
                saveUserName(name);
                startActivity(new Intent(this,WelcomeActivity.class));
                finish();
            }
        });
    }

    private void saveUserName(String name) {
        SharedPreferences preferences = getSharedPreferences(PREF_NAME,MODE_PRIVATE);
        preferences.edit().putString(KEY_USERNAME, name).apply();
    }
}
