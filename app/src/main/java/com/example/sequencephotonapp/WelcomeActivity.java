package com.example.sequencephotonapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        SharedPreferences preferences = getSharedPreferences(UserDetailsActivity.PREF_NAME,MODE_PRIVATE);

        String name = preferences.getString(UserDetailsActivity.KEY_USERNAME,"");

        TextView tvLabel = findViewById(R.id.tv_user_label);
        tvLabel.setText("Hello "+name);
        Button btnStart = findViewById(R.id.btn_start_game);
        LinearLayout container = findViewById(R.id.container);
        btnStart.setOnClickListener(view -> {
            container.setVisibility(View.VISIBLE);
            btnStart.setVisibility(View.GONE);
        });
        Button btnEasy = findViewById(R.id.btn_easy);
        Button btnHard = findViewById(R.id.btn_hard);

        btnEasy.setOnClickListener(view -> {
            startActivity(new Intent(WelcomeActivity.this,MainActivity.class));
        });
        btnHard.setOnClickListener(view -> {
            startActivity(new Intent(WelcomeActivity.this,MainActivity.class));
        });
    }
}
