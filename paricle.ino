// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
InternetButton b = InternetButton();


String inputData;
int totalseq;
int count;
bool started;
String display;
void setup() {
b.begin();

for(int i = 0;i< 3;i++)
   {
    b.allLedsOn(0,0,255);
    delay(400);
    b.allLedsOff();
    delay(400);
   }
Particle.function("sequence", repeat);
inputData = "";
count = 0;
display = "";
 // 8 is a 1/8th note and 4 is a 1/4 note
 b.playSong("C4,8,E4,8,G4,8,C5,8,G5,4");
}


void loop() {
	   if(b.allButtonsOn()){
        b.rainbow(10);
        b.playSong("E5,8,G5,8,E6,8,C6,8,D6,8,G6,8");
        b.allLedsOff();
    }

    if(b.buttonOn(1)){
        // playNote just plays a single note, and takes
        // a note name as a string and a note type as an int
        // Note types define duration like in scored music, so
        // 8 is a 1/8th note and 4 is a 1/4 note
        b.playNote("G3",8);
    }

    if(b.buttonOn(2)){
        b.playNote("G4",8);
    }

    if(b.buttonOn(3)){
        b.playNote("G5",8);
    }

    if(b.buttonOn(4)){
        b.playNote("G6",8);
    }
	 pressButton();

	}


void  pressButton(){
if(b.buttonOn(1)){
if(count < 4){
inputData = inputData + "A";
count = count + 1;
Particle.publish("userInput", inputData);
	  }
	 }
if(b.buttonOn(2)){
if(count < 4){
inputData = inputData + "B";
count = count + 1;
Particle.publish("userInput", inputData);
}
}
 if(b.buttonOn(3)){
if(count < 4){
inputData = inputData + "C";
count = count + 1;
Particle.publish("userInput", inputData);
}
}
if(b.buttonOn(4)){
if(count < 4){
inputData = inputData + "D";
count = count + 1;
Particle.publish("userInput", inputData);
}}

}

int sequencedisplay(int sum) {
return random(1, sum+1);
	}

int repeat(String status){
 if(status == "start"){
display = "";
count = 0;
started = true;
totalseq = 0;
for (int i = 1; i <= 4; i++){
if(started == true){

int no = sequencedisplay(4);
switch(no) {
 case 1: display = display + "A";
 break;
case 2: display = display + "B";
 break;
case 3:display = display + "C";
break;
case 4:display = display + "D";
break;
}
totalseq = totalseq + 1;
	            }   }

	 Particle.publish("count", String(totalseq));
	 if(totalseq == 4){
	   Particle.publish("output", inputData);
	   }
 }
	    else
 {
	  started = false;
}
return 1;
}

void score(){
totalseq = 0;
if(count == 4 && totalseq == 4&&count == totalseq&&display == inputData ){
if(display == inputData){
Particle.publish("player wins the game");
} else {
Particle.publish("player losses the game");
}
}
score();
}









